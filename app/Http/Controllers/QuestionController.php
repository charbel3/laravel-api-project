<?php

namespace App\Http\Controllers;

use App\Models\Question;
use Illuminate\Http\Request;

class QuestionController extends Controller
{
    public function index()
    {
        $questions = auth()->user()->company->questions;
        return response()->json($questions);
    }

    public function store(Request $request)
    {
        // Validation can be added here
        $question = auth()->user()->company->questions()->create($request->all());
        return response()->json($question, 201);
    }

    public function show(Question $question)
    {
        return response()->json($question);
    }

    public function update(Request $request, Question $question)
    {
        // Validation can be added here
        $question->update($request->all());
        return response()->json($question);
    }

    public function destroy(Question $question)
    {
        $question->delete();
        return response()->json(null, 204);
    }
}
