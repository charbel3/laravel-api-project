<?php

use App\Models\User;
use App\Models\File;
use Illuminate\Foundation\Testing\RefreshDatabase;

class AccessControlTest extends TestCase
{
    use RefreshDatabase;

    protected $admin, $company1, $company2;

    protected function setUp(): void
    {
        parent::setUp();

        // Create an admin and two companies
        $this->admin = User::factory()->create(['type' => 'admin']);
        $this->company1 = User::factory()->create(['type' => 'company']);
        $this->company2 = User::factory()->create(['type' => 'company']);
        
        // Additional setup as needed
    }

    public function test_admin_cannot_access_company_files()
    {
        $response = $this->actingAs($this->admin)->get('/api/files');
        $response->assertStatus(403);
    }

    public function test_company_cannot_access_other_companys_files()
    {
        $file = File::factory()->create(['company_id' => $this->company2->id]);
        $response = $this->actingAs($this->company1)->get("/api/files/{$file->id}");
        $response->assertStatus(403);
    }

    public function test_guest_cannot_access_files()
    {
        $response = $this->get('/api/files');
        $response->assertRedirect('login');
    }
}
