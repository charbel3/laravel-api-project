<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckOwnership
{
    public function handle(Request $request, Closure $next, $type)
    {
        $user = Auth::user();

        if ($user->type === 'admin') {
            return $next($request); // Admin has access to everything
        }

        if ($type === 'files' || $type === 'questions') {
            $company_id = $user->company->id;
            $resource_company_id = $request->$type->company_id;

            if ($company_id !== $resource_company_id) {
                return response()->json(['error' => 'Unauthorized'], 403);
            }
        }

        return $next($request);
    }
}
