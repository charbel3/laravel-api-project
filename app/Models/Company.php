<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Billable;

class Company extends Model
{
    use HasFactory, Billable;

    protected $fillable = [
        'name',
        'email',
        // other fields as needed
    ];

    public function files()
    {
        return $this->hasMany(File::class);
    }

    public function questions()
    {
        return $this->hasMany(Question::class);
    }

    // ...
}

