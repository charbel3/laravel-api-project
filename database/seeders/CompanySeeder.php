<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Company;

class CompanySeeder extends Seeder
{
    public function run()
    {
        \App\Models\Company::factory(10)->create()->each(function ($company) {
            $company->files()->saveMany(\App\Models\File::factory(5)->make());
            $company->questions()->saveMany(\App\Models\Question::factory(5)->make());
        });
    }
}
