<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

class FileFactory extends Factory
{
    protected $model = File::class;

    public function definition()
    {
        return [
            'path' => $this->faker->word . '.' . $this->faker->fileExtension,
            'original_name' => $this->faker->word . '.' . $this->faker->fileExtension,
            'mime_type' => $this->faker->mimeType,
            'company_id' => \App\Models\Company::factory(),
        ];
    }
}
