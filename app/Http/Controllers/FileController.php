<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;

class FileController extends Controller
{
    public function index()
    {
        $files = auth()->user()->company->files;
        return response()->json($files);
    }

    public function store(Request $request)
    {
        // Validation and file handling logic here
        $file = auth()->user()->company->files()->create($request->all());
        return response()->json($file, 201);
    }

    public function show(File $file)
    {
        return response()->json($file);
    }

    public function update(Request $request, File $file)
    {
        // Validation and file handling logic here
        $file->update($request->all());
        return response()->json($file);
    }

    public function destroy(File $file)
    {
        $file->delete();
        return response()->json(null, 204);
    }
}
