<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;

    protected $fillable = [
        'question_text',
        'answer_text',
        'company_id',
        'file_id',
    ];

    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    public function file()
    {
        return $this->belongsTo(File::class);
    }

    // ...
}

