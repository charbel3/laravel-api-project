<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    use HasFactory;

    protected $fillable = [
        'name', // Or any other fields relevant to your session
        'company_id', // Foreign key
        // Add other relevant fields
    ];

    /**
     * Get the company that owns the session.
     */
    public function company()
    {
        return $this->belongsTo(Company::class);
    }

    /**
     * Get the questions asked in this session.
     */
    public function questions()
    {
        return $this->hasMany(Question::class);
    }
}
