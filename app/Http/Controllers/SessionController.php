<?php

namespace App\Http\Controllers;

use App\Models\Session;
use Illuminate\Http\Request;

class SessionController extends Controller
{
    /**
     * Display a listing of sessions.
     */
    public function index()
    {
        $sessions = Session::all();
        return response()->json($sessions);
    }

    /**
     * Store a newly created session in storage.
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required|string|max:255',
            'company_id' => 'required|integer|exists:companies,id',
            // Validate other input fields
        ]);

        $session = Session::create($data);
        return response()->json($session);
    }

    /**
     * Display the specified session.
     */
    public function show(Session $session)
    {
        return response()->json($session);
    }

    /**
     * Update the specified session in storage.
     */
    public function update(Request $request, Session $session)
    {
        $data = $request->validate([
            'name' => 'sometimes|required|string|max:255',
            // Validate other fields you want to be updatable
        ]);

        $session->update($data);
        return response()->json($session);
    }

    /**
     * Remove the specified session from storage.
     */
    public function destroy(Session $session)
    {
        $session->delete();
        return response()->json(['message' => 'Session deleted successfully']);
    }
}
