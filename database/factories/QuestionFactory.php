<?php

namespace Database\Factories;

use App\Models\Question;
use App\Models\Company;
use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

class QuestionFactory extends Factory
{
    protected $model = Question::class;

    public function definition()
    {
        return [
            'question_text' => $this->faker->sentence(),
            'answer_text' => $this->faker->paragraph(),
            'company_id' => Company::factory(),
            'file_id' => File::factory(),
        ];
    }
}
